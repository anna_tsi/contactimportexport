﻿using System;
using System.Collections.Generic;
//using System.IO;
using System.Linq;
//using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Excel_Import.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = null;
            Session["username"] = null;
            Session["password"] = null;
            return View("LoginPage");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
       

        [HttpPost]
        public ActionResult LoginPage(FormCollection collection)
        {
            string username = collection.Get("UserName");
            string Password = collection.Get("Password");
            if ( username== "Admin" && Password == "Admin@12")
            {
                //Response.Redirect("http://103.104.124.146:5005/Excel/Index");
                Session["username"] = username;
                Session["password"] = Password;
                Response.Redirect("~/Excel/Index");
            }
            else
            {
                ViewBag.Message = "Please enter valid Username and Password";

            }
            return View("LoginPage");
        }

        
        //public ActionResult IcsResult()
        //{
        //    HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("https://calendar.google.com/calendar/ical/anbuk33%40gmail.com/private-2831297888578cb74378f356c764c01e/basic.ics");
        //    myRequest.Method = "GET";
        //    WebResponse myResponse = myRequest.GetResponse();
        //    StreamReader sr = new StreamReader(myResponse.GetResponseStream(), System.Text.Encoding.UTF8);
        //    string result = sr.ReadToEnd();
        //    Console.WriteLine(result);
        //    sr.Close();
        //    myResponse.Close();
        //    return View("LoginPage");
        //}
    }
}