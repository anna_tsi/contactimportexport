﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using System.Web.Mvc;
using Dapper;
using DocumentFormat.OpenXml;
using Excel_Import.Models;
using ExcelDataReader;
using GemBox.Spreadsheet;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web;
//using OfficeOpenXml;
//using OfficeOpenXml.Style;
//using System.Web.Script.Serialization;
//using DocumentFormat.OpenXml.Spreadsheet;
//using DocumentFormat.OpenXml.Packaging;
using ClosedXML.Excel;

namespace Excel_Import.Controllers
{
    public class ExcelController : Controller
    {
        Contacts obj = new Contacts();
        // GET: Excel     
        public ActionResult Index()
        {
            if (Session["username"] != null && Session["password"] != null)

            {
                return View();
            }
            else
            {
                Response.Redirect("~/Home/Index");
            }
            return View();
        }
        public ActionResult ContactsListView()
        {
            if (Session["username"] != null && Session["password"] != null)
            {
                return View();
            }
            else
            {
                Response.Redirect("~/Home/Index");
            }
            return View();
        }
        static IDbConnection dbExcel = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnStringExcel"].ConnectionString);

        public JsonResult ContactsListView_Read()

        {
            List<Contacts> _Contacts = new List<Contacts>();
            try
            {
                using (var db = dbExcel)
                {
                    var param = new DynamicParameters();
                    param.Add("ipMode", "GetAll");
                    param.Add("ipContactId", 0);
                    param.Add("ipCompany", null);
                    param.Add("ipLastName", null);
                    param.Add("ipFirstName", null);
                    param.Add("ipTitle", null);
                    param.Add("ipPhone1", null);
                    param.Add("ipPhone2", null);
                    param.Add("ipEmail", null);
                    param.Add("ipAddress", null);
                    param.Add("ipStreet", null);
                    param.Add("ipCity", null);
                    param.Add("ipState", null);
                    param.Add("ipzip", null);
                    param.Add("ipKeywords", null);
                    param.Add("ipCompanyType", null);
                    param.Add("ipPersonType", null);
                    param.Add("outLID", direction: ParameterDirection.Output);

                    using (var mpresults = db.QueryMultiple("contactdetails_crud", param, commandType: System.Data.CommandType.StoredProcedure))
                    {
                        _Contacts = mpresults.Read<Contacts>().ToList();
                        db.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"ContactsListView_Read => {ex}");
            }
            return Json(_Contacts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteContact(int a)
        {
            List<Contacts> _Contacts = new List<Contacts>();
            try
            {
                using (var db = dbExcel)
                {
                    var param = new DynamicParameters();
                    param.Add("ipMode", "Delete");
                    param.Add("ipContactId", a);
                    param.Add("ipCompany", null);
                    param.Add("ipLastName", null);
                    param.Add("ipFirstName", null);
                    param.Add("ipTitle", null);
                    param.Add("ipPhone1", null);
                    param.Add("ipPhone2", null);
                    param.Add("ipEmail", null);
                    param.Add("ipAddress", null);
                    param.Add("ipStreet", null);
                    param.Add("ipCity", null);
                    param.Add("ipState", null);
                    param.Add("ipzip", null);
                    param.Add("ipKeywords", null);
                    param.Add("ipCompanyType", null);
                    param.Add("ipPersonType", null);
                    param.Add("outLID", direction: ParameterDirection.Output);

                    using (var mpresults = db.QueryMultiple("contactdetails_crud", param, commandType: System.Data.CommandType.StoredProcedure))
                    {
                        //_Contacts = mpresults.Read<Contacts>().ToList();
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"DeleteContact => {ex}");
            }
            return Json(_Contacts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAdditionalDetails(int id)
        {
            var result = "Failed";
            using (var db = dbExcel)
            {
                var param1 = new DynamicParameters();

                param1.Add("ipMode", "Deleteadditionaldetails");
                param1.Add("ipId", id);
                param1.Add("ipContactId", 0);
                param1.Add("ipColumnName", null);
                param1.Add("ipValue", null);
                param1.Add("ipRowNumber", null);
                param1.Add("outLID", direction: ParameterDirection.Output);
                try
                {
                    var mpresults1 = db.Execute("addtionaldetails_crud", param1, commandType: System.Data.CommandType.StoredProcedure);
                    if (mpresults1 > 0)
                        result = "Success";
                    db.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"UpdateAdditionalDetails => {ex}");
                }

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateContact(Contacts obj)
        {
            List<Contacts> _Contacts = new List<Contacts>();
            bool issuccess = false;
            try
            {
                using (var db = dbExcel)
                {

                    var param = new DynamicParameters();
                    param.Add("ipMode", "Update");
                    param.Add("ipContactId", obj.ContactId);
                    param.Add("ipCompany", obj.Company);
                    param.Add("ipLastName", obj.LastName);
                    param.Add("ipFirstName", obj.FirstName);
                    param.Add("ipTitle", obj.Title);
                    param.Add("ipPhone1", obj.Phone1);
                    param.Add("ipPhone2", obj.Phone2);
                    param.Add("ipEmail", obj.Email);
                    param.Add("ipAddress", obj.Address);
                    param.Add("ipStreet", obj.Street);
                    param.Add("ipCity", obj.City);
                    param.Add("ipState", obj.State);
                    param.Add("ipzip", obj.Zip);
                    param.Add("ipKeywords", obj.KeyWords);
                    param.Add("ipCompanyType", obj.CompanyType);
                    param.Add("ipPersonType", obj.PersonType);
                    param.Add("outLID", direction: ParameterDirection.Output);

                    using (var mpresults = db.QueryMultiple("contactdetails_crud", param, commandType: System.Data.CommandType.StoredProcedure))
                    {

                        var id = param.Get<int>("outLID");
                        if (id > 0)
                        {
                            issuccess = true;
                        }
                    }
                }
                if (issuccess)
                    UpdateAdditionalDetails(obj.AdditionalDetails);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"UpdateContact => {ex}");
            }
            return RedirectToAction("ContactsListView");
        }
        public bool UpdateAdditionalDetails(List<AdditionalDetails> additionalDetails)
        {
            bool IsSuccess = false;

            using (var db = dbExcel)
            {
                foreach (var contact in additionalDetails)
                {
                    int additionaldetailsid = 0;
                    var param1 = new DynamicParameters();
                    var addtionaldata = GetAddtionalDetails(contact.ContactId);
                    if (addtionaldata.Count > 0)
                        additionaldetailsid = addtionaldata.Where(x => x.ContactId == contact.ContactId && x.ColumnName.ToLower().Equals(contact.ColumnName.ToLower())).Select(x => x.Id).FirstOrDefault();

                    if (!string.IsNullOrEmpty(contact.ColumnName) && !string.IsNullOrEmpty(contact.ColumnValue) && contact.ContactId != 0)
                    {
                        if (contact.Id == 0 && additionaldetailsid == 0)
                        {
                            param1.Add("ipMode", "InsertAdditionalDetails");
                        }
                        else
                        {
                            param1.Add("ipMode", "Updateaddtionaldetailsbycontactid");
                        }
                        param1.Add("ipId", contact.Id);
                        param1.Add("ipContactId", contact.ContactId);
                        param1.Add("ipColumnName", contact.ColumnName);
                        param1.Add("ipValue", contact.ColumnValue);
                        param1.Add("ipRowNumber", contact.RowNumber);
                        param1.Add("outLID", direction: ParameterDirection.Output);
                        try
                        {
                            using (var mpresults1 = db.QueryMultiple("addtionaldetails_crud", param1, commandType: System.Data.CommandType.StoredProcedure))
                            {
                                var result = mpresults1.ToString();

                                IsSuccess = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"UpdateAdditionalDetails => {ex}");
                        }
                    }
                }
            }
            return IsSuccess;
        }


        public ActionResult AddAdditionalDetails(List<AdditionalDetails> additionalDetails)
        {
            var result = "failed";
            using (var db = dbExcel)
            {
                foreach (var contact in additionalDetails)
                {
                    int additionaldetailsid = 0;
                    var param1 = new DynamicParameters();
                    var addtionaldata = GetAddtionalDetails(contact.ContactId);
                    if (addtionaldata.Count > 0)
                        additionaldetailsid = addtionaldata.Where(x => x.ContactId == contact.ContactId && x.ColumnName.ToLower().Equals(contact.ColumnName.ToLower())).Select(x => x.Id).FirstOrDefault();

                    if (!string.IsNullOrEmpty(contact.ColumnName) && !string.IsNullOrEmpty(contact.ColumnValue) && contact.ContactId != 0)
                    {
                        if (contact.Id == 0 && additionaldetailsid == 0)
                        {
                            param1.Add("ipMode", "InsertAdditionalDetails");
                        }
                        else
                        {
                            param1.Add("ipMode", "Updateaddtionaldetailsbycontactid");
                        }
                        param1.Add("ipId", contact.Id);
                        param1.Add("ipContactId", contact.ContactId);
                        param1.Add("ipColumnName", contact.ColumnName);
                        param1.Add("ipValue", contact.ColumnValue);
                        param1.Add("ipRowNumber", contact.RowNumber);
                        param1.Add("outLID", direction: ParameterDirection.Output);
                        try
                        {
                            var mpresults1 = db.Execute("addtionaldetails_crud", param1, commandType: System.Data.CommandType.StoredProcedure);

                            if (mpresults1 > 0)
                                result = "Success";
                            db.Close();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"UpdateAdditionalDetails => {ex}");
                        }
                    }
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AdditionalFieldview(int a)
        {
            List<AdditionalDetails> _Contacts = new List<AdditionalDetails>();
            try
            {
                using (var db = dbExcel)
                {
                    var param = new DynamicParameters();
                    param.Add("ipMode", "Getaddtionaldetailsbycontactid");
                    param.Add("ipId", null);
                    param.Add("ipContactId", a);
                    param.Add("ipColumnName", null);
                    param.Add("ipValue", null);
                    param.Add("ipRowNumber", null);
                    param.Add("outLID", direction: ParameterDirection.Output);

                    using (var mpresults = db.QueryMultiple("addtionaldetails_crud", param, commandType: System.Data.CommandType.StoredProcedure))
                    {
                        _Contacts = mpresults.Read<AdditionalDetails>().ToList();
                    }
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"AdditionalFieldview => {ex}");
            }
            return Json(_Contacts, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ContactUpdateview(int a)
        {
            Contacts _Contacts = new Contacts();
            try
            {
                using (var db = dbExcel)
                {
                    var param = new DynamicParameters();
                    param.Add("ipMode", "GetById");
                    param.Add("ipContactId", a);
                    param.Add("ipCompany", null);
                    param.Add("ipLastName", null);
                    param.Add("ipFirstName", null);
                    param.Add("ipTitle", null);
                    param.Add("ipPhone1", null);
                    param.Add("ipPhone2", null);
                    param.Add("ipEmail", null);
                    param.Add("ipAddress", null);
                    param.Add("ipStreet", null);
                    param.Add("ipCity", null);
                    param.Add("ipState", null);
                    param.Add("ipzip", null);
                    param.Add("ipKeywords", null);
                    param.Add("ipCompanyType", null);
                    param.Add("ipPersonType", null);
                    param.Add("outLID", direction: ParameterDirection.Output);

                    using (var mpresults = db.QueryMultiple("contactdetails_crud", param, commandType: System.Data.CommandType.StoredProcedure))
                    {
                        _Contacts = mpresults.Read<Contacts>().FirstOrDefault();
                    }
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"ContactUpdateview => {ex}");
            }
            return Json(_Contacts, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Contactdetailview(int a)
        {
            List<Contacts> _Contacts = new List<Contacts>();
            try
            {
                using (var db = dbExcel)
                {
                    var param = new DynamicParameters();
                    param.Add("ipMode", "GetById");
                    param.Add("ipContactId", a);
                    param.Add("ipCompany", null);
                    param.Add("ipLastName", null);
                    param.Add("ipFirstName", null);
                    param.Add("ipTitle", null);
                    param.Add("ipPhone1", null);
                    param.Add("ipPhone2", null);
                    param.Add("ipEmail", null);
                    param.Add("ipAddress", null);
                    param.Add("ipStreet", null);
                    param.Add("ipCity", null);
                    param.Add("ipState", null);
                    param.Add("ipzip", null);
                    param.Add("ipKeywords", null);
                    param.Add("ipCompanyType", null);
                    param.Add("ipPersonType", null);
                    param.Add("outLID", direction: ParameterDirection.Output);

                    using (var mpresults = dbExcel.QueryMultiple("contactdetails_crud", param, commandType: System.Data.CommandType.StoredProcedure))
                    {
                        _Contacts = mpresults.Read<Contacts>().ToList();
                    }
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Contactdetailview => {ex}");
            }
            return Json(_Contacts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ContactsRead([DataSourceRequest] DataSourceRequest request)
        {
            List<Contacts> _Contacts = new List<Contacts>();
            try
            {

                var param = new DynamicParameters();
                param.Add("ipMode", "GetAll");
                param.Add("ipContactId", 0);
                param.Add("ipCompany", null);
                param.Add("ipLastName", null);
                param.Add("ipFirstName", null);
                param.Add("ipTitle", null);
                param.Add("ipPhone1", null);
                param.Add("ipPhone2", null);
                param.Add("ipEmail", null);
                param.Add("ipAddress", null);
                param.Add("ipStreet", null);
                param.Add("ipCity", null);
                param.Add("ipState", null);
                param.Add("ipzip", null);
                param.Add("ipKeywords", null);
                param.Add("ipCompanyType", null);
                param.Add("ipPersonType", null);
                param.Add("outLID", direction: ParameterDirection.Output);

                //var sqlStatement = @"SELECT * FROM contactdetails";
                //using (var connection = new MySqlConnection("Server =103.213.192.114;Port=3306;Database=importexportexceldata;Uid=admin;Pwd=vss2@18;"))
                //{
                //    _Contacts = connection.Query<Contacts>(sqlStatement).ToList();
                //}
                try
                {

                    using (var mpresults = dbExcel.QueryMultiple("contactdetails_crud", param, commandType: System.Data.CommandType.StoredProcedure))
                    {
                        _Contacts = mpresults.Read<Contacts>().ToList();

                    }
                    dbExcel.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"ContactsRead => {ex}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"ContactsRead => {ex}");
            }
            return Json(_Contacts.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public List<Contacts> GetContactsdetails()

        {
            List<Contacts> _Contacts = new List<Contacts>();
            try
            {
                var param = new DynamicParameters();
                param.Add("ipMode", "GetAll");
                param.Add("ipContactId", 0);
                param.Add("ipCompany", null);
                param.Add("ipLastName", null);
                param.Add("ipFirstName", null);
                param.Add("ipTitle", null);
                param.Add("ipPhone1", null);
                param.Add("ipPhone2", null);
                param.Add("ipEmail", null);
                param.Add("ipAddress", null);
                param.Add("ipStreet", null);
                param.Add("ipCity", null);
                param.Add("ipState", null);
                param.Add("ipzip", null);
                param.Add("ipKeywords", null);
                param.Add("ipCompanyType", null);
                param.Add("ipPersonType", null);
                param.Add("outLID", direction: ParameterDirection.Output);

                //var sqlStatement = @"SELECT * FROM contactdetails";
                //using (var connection = new MySqlConnection("Server =103.213.192.114;Port=3306;Database=importexportexceldata;Uid=admin;Pwd=vss2@18;"))
                //{
                //    _Contacts = connection.Query<Contacts>(sqlStatement).ToList();
                //}
                try
                {
                    using (var mpresults = dbExcel.QueryMultiple("contactdetails_crud", param, commandType: System.Data.CommandType.StoredProcedure))
                    {
                        _Contacts = mpresults.Read<Contacts>().ToList();
                        dbExcel.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"GetContactsdetails => {ex}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"GetContactsdetails => {ex}");
            }
            return _Contacts;
        }

        public List<AdditionalDetails> GetAddtionalDetails(int contactid)
        {
            List<AdditionalDetails> _addtionaldetails = new List<AdditionalDetails>();
            try
            {
                var param = new DynamicParameters();
                param.Add("ipMode", "Getaddtionaldetailsbycontactid");
                param.Add("ipId", 0);
                param.Add("ipContactId", contactid);
                param.Add("ipColumnName", null);
                param.Add("ipRowNumber", null);
                param.Add("ipValue", null);
                param.Add("outLID", direction: ParameterDirection.Output);
                try
                {
                    using (var mpresults = dbExcel.QueryMultiple("addtionaldetails_crud", param, commandType: System.Data.CommandType.StoredProcedure))
                    {
                        _addtionaldetails = mpresults.Read<AdditionalDetails>().ToList();
                        dbExcel.Close();
                    }
                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetAddtionalDetail - " + ex.Message);
            }
            return _addtionaldetails;
        }

        public ActionResult AllContactExcelExport()
        {
            List<Contacts> _Contacts = new List<Contacts>();

            var response = "";
            DataTable dt = ContactswithaddtionaldetailsRead();
            var fileName = $"ContactList_{Guid.NewGuid()}.xlsx";
            //Name of File  
            try
            {
                using (XLWorkbook wb = new XLWorkbook())
                {
                    //Add DataTable in worksheet  
                    wb.Worksheets.Add(dt,"ContactInfo");
                    var directory = Server.MapPath("~/Files/");
                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);


                    var filePath = $"{directory}{fileName}";
                    wb.SaveAs(filePath);
                }
            }




            //SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
            //var workbook = new ExcelFile();
            //var worksheet = workbook.Worksheets.Add("DataTable to Sheet");

            //// Insert DataTable to an Excel worksheet.
            //worksheet.InsertDataTable(dt,
            //    new InsertDataTableOptions()
            //    {
            //        ColumnHeaders = true,
            //        StartRow = 0
            //    });

            //try
            //{
            //    workbook.Save(filePath);
            //}
            catch (Exception ex)
            {
                Console.WriteLine("issue while save workbook" + ex.Message);
            }
            var fileurl = $"{ ConfigurationManager.AppSettings["BaseURL"]}Files/{fileName}";
            response = $"{fileurl}";
            return Content(response);
        }

        public ActionResult UploadContacts(IEnumerable<HttpPostedFileBase> files)
        {
            try
            {

                if (files != null)
                {
                    foreach (var file in files)
                    {
                        DataTable dt = new DataTable();
                        IExcelDataReader excelReader;
                        DataSet ds = new DataSet();
                        var objcontactdetails = new Contacts();

                        string strFileType = Path.GetExtension(file.FileName).ToLower();

                        string path = Server.MapPath("~/UploadedContactDetailsFile");
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string filePath = Path.Combine(path, "contact_" + DateTime.Now.ToString("yyyy-MM-d--HH-mm-ss") + strFileType);
                        file.SaveAs(filePath);
                        #region Old Code
                        //using (FileStream stream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read))
                        //{
                        //    if (strFileType.Trim() == ".xls")
                        //        excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                        //    else
                        //        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                        //    ds = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                        //    {
                        //        ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                        //        {
                        //            UseHeaderRow = true
                        //        }
                        //    });
                        //}

                        // Create a file stream containing the Excel file to be opened
                        #endregion
                        #region using OpenXml


                        //Open the Excel file in Read Mode using OpenXml.
                        //try
                        //{
                        //    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(filePath, false))
                        //    {
                        //        //Read the first Sheet from Excel file.
                        //        Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();

                        //        //Get the Worksheet instance.
                        //        Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;

                        //        //Fetch all the rows present in the Worksheet.
                        //        IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();

                        //        //Create a new DataTable.
                        //        // DataTable dt = new DataTable();

                        //        //Loop through the Worksheet rows.
                        //        foreach (Row row in rows)
                        //        {
                        //            //Use the first row to add columns to DataTable.
                        //            if (row.RowIndex.Value == 1)
                        //            {
                        //                foreach (Cell cell in row.Descendants<Cell>())
                        //                {
                        //                    //var res = GetValue(doc, cell);
                        //                    //if(res!="")
                        //                    dt.Columns.Add(GetValue(doc, cell));
                        //                }
                        //            }
                        //            else
                        //            {
                        //                //Add rows to DataTable.
                        //                dt.Rows.Add();
                        //                int i = 0;
                        //                for (int j = 0; j < dt.Columns.Count; j++)
                        //                {
                        //                    var res = string.Empty;
                        //                    if (row.Descendants<Cell>().Count() > j)
                        //                    {
                        //                        res = GetValue(doc, row.Descendants<Cell>().ElementAt(i));
                        //                        if (res != null)
                        //                        {
                        //                            dt.Rows[dt.Rows.Count - 1][j] = res;
                        //                            i++;
                        //                        }
                        //                        else
                        //                        {
                        //                            dt.Rows[dt.Rows.Count - 1][j] = res;
                        //                        }
                        //                    }
                        //                    else
                        //                    {
                        //                        //res = GetValue(doc, row.Descendants<Cell>().ElementAt(i));
                        //                        //if (res != null)
                        //                        //{
                        //                        //    dt.Rows[dt.Rows.Count - 1][j] = res;
                        //                        //    i++;
                        //                        //}
                        //                        //else
                        //                        dt.Rows[dt.Rows.Count - 1][j] = res;
                        //                    }


                        //                }
                        //            }
                        //        }
                        //    }
                        //}
                        //catch(Exception ex)
                        //{
                        //    Console.WriteLine(ex.Message);

                        //}
                        #endregion
                        #region using ClosedXML
                        using (XLWorkbook workBook = new XLWorkbook(filePath))
                        {
                            //Read the first Sheet from Excel file.
                            IXLWorksheet workSheet = workBook.Worksheet(1);

                            //Create a new DataTable.
                            //DataTable dt = new DataTable();

                            //Loop through the Worksheet rows.
                            bool firstRow = true;
                            foreach (IXLRow row in workSheet.Rows())
                            {
                                //Use the first row to add columns to DataTable.
                                if (firstRow)
                                {
                                    foreach (IXLCell cell in row.Cells())
                                    {
                                        dt.Columns.Add(Regex.Replace(cell.Value.ToString(), @"[^0-9a-zA-Z]+", ""));
                                    }
                                    firstRow = false;
                                }
                                else
                                {
                                    //Add rows to DataTable.
                                    dt.Rows.Add();
                                    int i = 0;
                                    foreach (IXLCell cell in row.Cells(1, dt.Columns.Count))
                                    {
                                        dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                                        i++;
                                    }
                                }
                            }
                        }
                        #endregion
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        }
                        // dt = ds.Tables[0];
                        int columnStartIndex = 0;
                        string header = "Column";
                        for (int i = dt.Columns.Count - 1; i >= columnStartIndex; i--)
                        {
                            DataColumn col = dt.Columns[i];
                            //if (dt.AsEnumerable().All(r => r.IsNull(col) || string.IsNullOrWhiteSpace(r[col].ToString())))
                            if (col.ColumnName.Contains(header))
                                dt.Columns.RemoveAt(i);
                        }
                        int rowStartIndex = 0;
                        
                        for (int i = dt.Rows.Count - 1; i >= rowStartIndex; i--)
                        {
                            int rowemptycount = 0;
                            for (int j = 0; j < dt.Rows[i].ItemArray.Length; j++)
                            {
                                var value = dt.Rows[i].ItemArray[j].ToString();
                                if (string.IsNullOrEmpty(dt.Rows[i].ItemArray[j].ToString()) || dt.Rows[i].ItemArray[j] == "")
                                    rowemptycount++;
                            }
                            if (rowemptycount == dt.Rows[i].ItemArray.Length)
                            {
                                dt.Rows.RemoveAt(i);
                                dt.AcceptChanges();
                            }
                        }

                        for (int i = dt.Columns.Count - 1; i >= columnStartIndex; i--)
                        {
                            Regex.Replace(Regex.Replace(dt.Columns[i].ToString(), @"\s", ""), @"[^0-9a-zA-Z]+", "");

                        }
                        foreach (DataColumn c in dt.Columns)
                            c.ColumnName = Regex.Replace(String.Join("", c.ColumnName.Split()), @"[^0-9a-zA-Z]+", "");
                        var data = JsonConvert.SerializeObject(dt, Formatting.None);
                        return Json(data);
                    }
                }
                return Content("");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        //private string GetValue(SpreadsheetDocument doc, Cell cell)
        //{
        //    string value = null;
        //    if (cell != null && cell.CellValue != null)
        //        value = cell.CellValue.InnerText;
        //    if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString && value != "")
        //    {
        //        return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
        //    }
        //    return value;
        //}
        public async Task<ActionResult> Checkduplicatedata(string gridData, string selectedColumn, string excelColumn)
        {
            try
            {
                var tablevalues = GetContactsdetails();
                var griddatatoken = JToken.Parse(JsonConvert.SerializeObject(gridData)).ToString();
                var selectedcolumnjtoken = JToken.Parse(selectedColumn).ToArray();
                var excelCol = JToken.Parse(excelColumn).ToArray();

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                List<int> nonDuplicateIndex = new List<int>();

                var data = JsonConvert.DeserializeObject<DataTable>(griddatatoken);
                dt = data;

                int index = 0;
                foreach (DataRow row in data.Rows)
                {
                    var objcontactdetails = new Contacts();
                    var objadditionaldetails = new List<AdditionalDetails>();

                    int inc = 0;
                    foreach (var columnselected in selectedcolumnjtoken)
                    {
                        var excelcolname = excelCol[inc].ToString();
                        if (columnselected.ToString().ToUpper().Equals("COMPANY")) { objcontactdetails.Company = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("LASTNAME")) { objcontactdetails.LastName = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("FIRSTNAME")) { objcontactdetails.FirstName = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("TITLE")) { objcontactdetails.Title = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("PHONE1")) { objcontactdetails.Phone1 = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("PHONE2")) { objcontactdetails.Phone2 = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("EMAIL")) { objcontactdetails.Email = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("KEYWORDS")) { objcontactdetails.KeyWords = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("ADDRESS")) { objcontactdetails.Address = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("STREET")) { objcontactdetails.Street = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("CITY")) { objcontactdetails.City = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("STATE")) { objcontactdetails.State = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("ZIP")) { objcontactdetails.Zip = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("COMPANYTYPE")) { objcontactdetails.CompanyType = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("PERSONTYPE")) { objcontactdetails.PersonType = Convert.ToString(row[excelcolname]).Trim(); }
                        else
                        {
                            if (data.Columns.Contains(columnselected.ToString().Trim()) && !string.IsNullOrEmpty(Convert.ToString(row[columnselected.ToString()]).Trim()))
                            {
                                var additionaldata = new AdditionalDetails();
                                additionaldata.ColumnName = columnselected.ToString();
                                additionaldata.ColumnValue = Convert.ToString(row[columnselected.ToString()]).Trim();
                                additionaldata.RowNumber = index;
                                objadditionaldetails.Add(additionaldata);
                            }
                        }
                        inc++;
                    }
                    #region old condition
                    //if (tablevalues.Any(u => !string.IsNullOrEmpty(u.Company) ?
                    //                                        u.Company.Equals(!string.IsNullOrEmpty(objcontactdetails.Company) ?
                    //                                        objcontactdetails.Company : null, StringComparison.OrdinalIgnoreCase) : false &&

                    //                                        !string.IsNullOrEmpty(u.Email) ?
                    //                                         u.Email.Equals(!string.IsNullOrEmpty(objcontactdetails.Email) ?
                    //                                       objcontactdetails.Email : null, StringComparison.OrdinalIgnoreCase) : false &&

                    //                                       !string.IsNullOrEmpty(u.FirstName) ?
                    //                                       u.FirstName.Equals(!string.IsNullOrEmpty(objcontactdetails.FirstName) ?
                    //                                       objcontactdetails.FirstName : null, StringComparison.OrdinalIgnoreCase) : false &&

                    //                                       !string.IsNullOrEmpty(u.LastName) ?
                    //                                       u.LastName.Equals(!string.IsNullOrEmpty(objcontactdetails.LastName) ?
                    //                                       objcontactdetails.LastName : null, StringComparison.OrdinalIgnoreCase) : false
                    //                                       ))
                    #endregion
                    if (tablevalues.Any(u => !string.IsNullOrEmpty(u.Company) ?
                                                         u.Company.Equals(!string.IsNullOrEmpty(objcontactdetails.Company) ?
                                                         objcontactdetails.Company : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.Company) &&

                                                         !string.IsNullOrEmpty(u.Email) ?
                                                          u.Email.Equals(!string.IsNullOrEmpty(objcontactdetails.Email) ?
                                                        objcontactdetails.Email : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.Email) &&

                                                        !string.IsNullOrEmpty(u.FirstName) ?
                                                        u.FirstName.Equals(!string.IsNullOrEmpty(objcontactdetails.FirstName) ?
                                                        objcontactdetails.FirstName : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.FirstName) &&

                                                        !string.IsNullOrEmpty(u.LastName) ?
                                                        u.LastName.Equals(!string.IsNullOrEmpty(objcontactdetails.LastName) ?
                                                        objcontactdetails.LastName : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.LastName)
                                                        ))
                    {
                        var contactdetails = tablevalues.Where(u => !string.IsNullOrEmpty(u.Company) ?
                                              u.Company.Equals(!string.IsNullOrEmpty(objcontactdetails.Company) ?
                                              objcontactdetails.Company : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.Company) &&

                                              !string.IsNullOrEmpty(u.Email) ?
                                               u.Email.Equals(!string.IsNullOrEmpty(objcontactdetails.Email) ?
                                             objcontactdetails.Email : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.Email) &&

                                             !string.IsNullOrEmpty(u.FirstName) ?
                                             u.FirstName.Equals(!string.IsNullOrEmpty(objcontactdetails.FirstName) ?
                                             objcontactdetails.FirstName : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.FirstName) &&

                                             !string.IsNullOrEmpty(u.LastName) ?
                                             u.LastName.Equals(!string.IsNullOrEmpty(objcontactdetails.LastName) ?
                                             objcontactdetails.LastName : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.LastName)
                                             ).Select(x => x).FirstOrDefault();
                        // var contactdetail = Contactdetailview(objcontactdetails.ContactId);
                        // var contactdetails = JsonConvert.DeserializeObject<Contacts>(contactdetail.ToString());
                        if (contactdetails != null && contactdetails.ContactId != 0)
                        {
                            if (objcontactdetails != null)
                            {
                                var param = new DynamicParameters();
                                param.Add("ipMode", "Update");
                                param.Add("ipContactId", contactdetails.ContactId);
                                param.Add("ipCompany", !string.IsNullOrEmpty(objcontactdetails.Company) ? objcontactdetails.Company : contactdetails.Company);
                                param.Add("ipLastName", !string.IsNullOrEmpty(objcontactdetails.LastName) ? objcontactdetails.LastName : contactdetails.LastName);
                                param.Add("ipFirstName", !string.IsNullOrEmpty(objcontactdetails.FirstName) ? objcontactdetails.FirstName : contactdetails.FirstName);
                                param.Add("ipTitle", !string.IsNullOrEmpty(objcontactdetails.Title) ? objcontactdetails.Title : contactdetails.Title);
                                param.Add("ipPhone1", !string.IsNullOrEmpty(objcontactdetails.Phone1) ? objcontactdetails.Phone1 : contactdetails.Phone1);
                                param.Add("ipPhone2", !string.IsNullOrEmpty(objcontactdetails.Phone2) ? objcontactdetails.Phone2 : contactdetails.Phone2);
                                param.Add("ipEmail", !string.IsNullOrEmpty(objcontactdetails.Email) ? objcontactdetails.Email : contactdetails.Email);
                                param.Add("ipAddress", !string.IsNullOrEmpty(objcontactdetails.Address) ? objcontactdetails.Address : contactdetails.Address);
                                param.Add("ipStreet", !string.IsNullOrEmpty(objcontactdetails.Street) ? objcontactdetails.Street : contactdetails.Street);
                                param.Add("ipCity", !string.IsNullOrEmpty(objcontactdetails.City) ? objcontactdetails.City : contactdetails.City);
                                param.Add("ipState", !string.IsNullOrEmpty(objcontactdetails.State) ? objcontactdetails.State : contactdetails.State);
                                param.Add("ipzip", !string.IsNullOrEmpty(objcontactdetails.Zip) ? objcontactdetails.Zip : contactdetails.Zip);
                                param.Add("ipKeywords", !string.IsNullOrEmpty(objcontactdetails.KeyWords) ? objcontactdetails.KeyWords : contactdetails.KeyWords);
                                param.Add("ipCompanyType", !string.IsNullOrEmpty(objcontactdetails.CompanyType) ? objcontactdetails.CompanyType : contactdetails.CompanyType);
                                param.Add("ipPersonType", !string.IsNullOrEmpty(objcontactdetails.PersonType) ? objcontactdetails.PersonType : contactdetails.PersonType);
                                param.Add("outLID", direction: ParameterDirection.Output);
                                try
                                {

                                    var mpresults = dbExcel.QueryMultiple("contactdetails_crud", param, commandType: System.Data.CommandType.StoredProcedure);
                                    dbExcel.Close();
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine($"Updateduplicatedata => {ex}");
                                }
                            }
                            var addtionaldata = GetAddtionalDetails(contactdetails.ContactId);
                            foreach (var addcolumn in objadditionaldetails)
                            {

                                var additionaldetails = (addtionaldata != null) ? addtionaldata.Where(x => x.ContactId == contactdetails.ContactId && x.ColumnName.ToString().ToLower().Equals(addcolumn.ColumnName.ToLower())).Select(x => x).FirstOrDefault() : null;
                                if (additionaldetails != null && additionaldetails.Id != 0)
                                {
                                    var param = new DynamicParameters();
                                    param.Add("ipMode", "Updateaddtionaldetailsbycontactid");
                                    param.Add("ipId", additionaldetails.Id);
                                    param.Add("ipContactId", additionaldetails.ContactId);
                                    param.Add("ipColumnName", !string.IsNullOrEmpty(addcolumn.ColumnName) ? addcolumn.ColumnName : additionaldetails.ColumnName);
                                    param.Add("ipValue", !string.IsNullOrEmpty(addcolumn.ColumnValue) ? addcolumn.ColumnValue : additionaldetails.ColumnValue);
                                    param.Add("ipRowNumber", addcolumn.RowNumber);
                                    param.Add("outLID", direction: ParameterDirection.Output);
                                    try
                                    {
                                        var mpresults = dbExcel.QueryMultiple("addtionaldetails_crud", param, commandType: System.Data.CommandType.StoredProcedure);
                                        dbExcel.Close();
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                                else
                                {

                                    if (addcolumn.ColumnValue != null)
                                    {
                                        var param = new DynamicParameters();
                                        param.Add("ipMode", "InsertAdditionalDetails");
                                        param.Add("ipId", 0);
                                        param.Add("ipContactId", contactdetails.ContactId);
                                        param.Add("ipColumnName", addcolumn.ColumnName);
                                        param.Add("ipValue", addcolumn.ColumnValue);
                                        param.Add("ipRowNumber", index);
                                        param.Add("outLID", direction: ParameterDirection.Output);
                                        try
                                        {
                                            var mpresults = dbExcel.QueryMultiple("addtionaldetails_crud", param, commandType: System.Data.CommandType.StoredProcedure);
                                            dbExcel.Close();
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                    }
                                }
                                if (dbExcel != null)
                                    dbExcel.Close();
                            }
                        }
                    
                }
                    else
                    {
                        nonDuplicateIndex.Add(index);
                        int contactid = 0;
                        if (objcontactdetails != null)
                        {
                            #region start insert query with stored procedure

                            var param = new DynamicParameters();
                            param.Add("ipMode", "Insert");
                            param.Add("ipContactId", 0);
                            param.Add("ipCompany", objcontactdetails.Company);
                            param.Add("ipLastName", objcontactdetails.LastName);
                            param.Add("ipFirstName", objcontactdetails.FirstName);
                            param.Add("ipTitle", objcontactdetails.Title);
                            param.Add("ipPhone1", objcontactdetails.Phone1);
                            param.Add("ipPhone2", objcontactdetails.Phone2);
                            param.Add("ipEmail", objcontactdetails.Email);
                            param.Add("ipAddress", objcontactdetails.Address);
                            param.Add("ipStreet", objcontactdetails.Street);
                            param.Add("ipCity", objcontactdetails.City);
                            param.Add("ipState", objcontactdetails.State);
                            param.Add("ipzip", objcontactdetails.Zip);
                            param.Add("ipKeywords", objcontactdetails.KeyWords);
                            param.Add("ipCompanyType", objcontactdetails.CompanyType);
                            param.Add("ipPersonType", objcontactdetails.PersonType);
                            param.Add("outLID", direction: ParameterDirection.Output);
                            try
                            {
                                using (var mpresults = dbExcel.QueryMultiple("contactdetails_crud", param, commandType: System.Data.CommandType.StoredProcedure))
                                {
                                    contactid = param.Get<Int32>("outLID");
                                    dbExcel.Close();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine($"Checkduplicatedata => {ex}");
                            }
                            #endregion

                            if (contactid != 0)
                            {
                                var addtionaldata = GetAddtionalDetails(contactid);
                                foreach (var addcolumn in objadditionaldetails)
                                {
                                    var additionaldetailsid = 0;
                                    if (addtionaldata.Count > 0)
                                        additionaldetailsid = addtionaldata.Where(x => x.ContactId == contactid && x.ColumnName.ToLower().Equals(addcolumn.ColumnName.ToLower())).Select(x => x.Id).FirstOrDefault();

                                    if (!string.IsNullOrEmpty(addcolumn.ColumnValue) && additionaldetailsid == 0)
                                    {
                                        var paramadd = new DynamicParameters();
                                        paramadd.Add("ipMode", "InsertAdditionalDetails");
                                        paramadd.Add("ipId", 0);
                                        paramadd.Add("ipContactId", contactid);
                                        paramadd.Add("ipColumnName", addcolumn.ColumnName);
                                        paramadd.Add("ipValue", addcolumn.ColumnValue);
                                        paramadd.Add("ipRowNumber", index);
                                        paramadd.Add("outLID", direction: ParameterDirection.Output);
                                        try
                                        {

                                            var mpresults = dbExcel.QueryMultiple("addtionaldetails_crud", paramadd, commandType: System.Data.CommandType.StoredProcedure);
                                            dbExcel.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine($"insertifnotduplicate => {ex}");
                                        }
                                    }
                                }
                            }

                        }
                    }
                    index++;
                }

                if (index > 0)
                    return Json(index);
                //if (nonDuplicateIndex.Count >= 0)
                //{
                //    for (int i = nonDuplicateIndex.Count; i > 0; i--)
                //    {

                //        dt.Rows[nonDuplicateIndex[i - 1]].Delete();
                //    }
                //    dt.AcceptChanges();
                //    var duplicatedata = JsonConvert.SerializeObject(dt, Formatting.None);
                //    return Json(duplicatedata);
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine($"insertifnotduplicate => {ex}");
                //return Json(ex.Message,JsonRequestBehavior.AllowGet);
            }

            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Updateduplicatedata(string updategridData, string selectedColumn, string excelColumn)
        {
            try
            {
                var tablevalues = GetContactsdetails();
                var griddatatoken = JToken.Parse(updategridData).ToString();
                var selectedcolumnjtoken = JToken.Parse(selectedColumn).ToArray();
                var excelCol = JToken.Parse(excelColumn).ToArray();

                var data = JsonConvert.DeserializeObject<DataTable>(griddatatoken);

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                List<int> nonDuplicateIndex = new List<int>();
                dt = data;

                foreach (DataColumn col in data.Columns)
                    col.ColumnName = col.ColumnName.Trim();


                int index = 0;
                foreach (DataRow row in data.Rows)
                {
                    var objcontactdetails = new Contacts();
                    var objadditionaldetails = new List<AdditionalDetails>();
                    int inc = 0;
                    foreach (var columnselected in selectedcolumnjtoken)
                    {
                        var excelcolname = excelCol[inc].ToString();
                        if (columnselected.ToString().ToUpper().Equals("COMPANY")) { objcontactdetails.Company = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("LASTNAME")) { objcontactdetails.LastName = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("FIRSTNAME")) { objcontactdetails.FirstName = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("TITLE")) { objcontactdetails.Title = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("PHONE1")) { objcontactdetails.Phone1 = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("PHONE2")) { objcontactdetails.Phone2 = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("EMAIL")) { objcontactdetails.Email = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("KEYWORDS")) { objcontactdetails.KeyWords = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("ADDRESS")) { objcontactdetails.Address = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("STREET")) { objcontactdetails.Street = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("CITY")) { objcontactdetails.City = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("STATE")) { objcontactdetails.State = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("ZIP")) { objcontactdetails.Zip = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("COMPANYTYPE")) { objcontactdetails.CompanyType = Convert.ToString(row[excelcolname]).Trim(); }
                        else if (columnselected.ToString().ToUpper().Equals("PERSONTYPE")) { objcontactdetails.PersonType = Convert.ToString(row[excelcolname]).Trim(); }
                        else
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(row[columnselected.ToString()]).Trim()) && data.Columns.Contains(columnselected.ToString().Trim()))
                            {
                                var additionaldata = new AdditionalDetails();
                                additionaldata.ColumnName = columnselected.ToString();
                                additionaldata.ColumnValue = Convert.ToString(row[columnselected.ToString()]).Trim();
                                additionaldata.RowNumber = index;
                                objadditionaldetails.Add(additionaldata);
                            }
                        }
                        inc++;
                    }
                    var selectdata = tablevalues;
                    //if (tablevalues.Any(u => !string.IsNullOrEmpty(u.Company) ?
                    //                                        u.Company.Contains(!string.IsNullOrEmpty(objcontactdetails.Company) ?
                    //                                        objcontactdetails.Company : null) :
                    //                                        !string.IsNullOrEmpty(u.Email) ?
                    //                                        !string.IsNullOrEmpty(objcontactdetails.Email) ?
                    //                                        u.Email.Contains(objcontactdetails.Email) : false : false))
                    if (tablevalues.Any(u => !string.IsNullOrEmpty(u.Company) ?
                                                u.Company.Equals(!string.IsNullOrEmpty(objcontactdetails.Company) ?
                                                objcontactdetails.Company : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.Company) &&

                                                !string.IsNullOrEmpty(u.Email) ?
                                                 u.Email.Equals(!string.IsNullOrEmpty(objcontactdetails.Email) ?
                                               objcontactdetails.Email : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.Email) &&

                                               !string.IsNullOrEmpty(u.FirstName) ?
                                               u.FirstName.Equals(!string.IsNullOrEmpty(objcontactdetails.FirstName) ?
                                               objcontactdetails.FirstName : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.FirstName) &&

                                               !string.IsNullOrEmpty(u.LastName) ?
                                               u.LastName.Equals(!string.IsNullOrEmpty(objcontactdetails.LastName) ?
                                               objcontactdetails.LastName : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.LastName)
                                               ))
                    {
                        var contactdetails = tablevalues.Where(u => !string.IsNullOrEmpty(u.Company) ?
                                                u.Company.Equals(!string.IsNullOrEmpty(objcontactdetails.Company) ?
                                                objcontactdetails.Company : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.Company) &&

                                                !string.IsNullOrEmpty(u.Email) ?
                                                 u.Email.Equals(!string.IsNullOrEmpty(objcontactdetails.Email) ?
                                               objcontactdetails.Email : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.Email) &&

                                               !string.IsNullOrEmpty(u.FirstName) ?
                                               u.FirstName.Equals(!string.IsNullOrEmpty(objcontactdetails.FirstName) ?
                                               objcontactdetails.FirstName : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.FirstName) &&

                                               !string.IsNullOrEmpty(u.LastName) ?
                                               u.LastName.Equals(!string.IsNullOrEmpty(objcontactdetails.LastName) ?
                                               objcontactdetails.LastName : null, StringComparison.OrdinalIgnoreCase) : string.IsNullOrEmpty(objcontactdetails.LastName)
                                               ).Select(x => x).FirstOrDefault();
                        // var contactdetail = Contactdetailview(objcontactdetails.ContactId);
                        // var contactdetails = JsonConvert.DeserializeObject<Contacts>(contactdetail.ToString());
                        if (contactdetails != null && contactdetails.ContactId != 0)
                        {
                            if (objcontactdetails != null)
                            {
                                var param = new DynamicParameters();
                                param.Add("ipMode", "Update");
                                param.Add("ipContactId", contactdetails.ContactId);
                                param.Add("ipCompany", !string.IsNullOrEmpty(objcontactdetails.Company) ? objcontactdetails.Company : contactdetails.Company);
                                param.Add("ipLastName", !string.IsNullOrEmpty(objcontactdetails.LastName) ? objcontactdetails.LastName : contactdetails.LastName);
                                param.Add("ipFirstName", !string.IsNullOrEmpty(objcontactdetails.FirstName) ? objcontactdetails.FirstName : contactdetails.FirstName);
                                param.Add("ipTitle", !string.IsNullOrEmpty(objcontactdetails.Title) ? objcontactdetails.Title : contactdetails.Title);
                                param.Add("ipPhone1", !string.IsNullOrEmpty(objcontactdetails.Phone1) ? objcontactdetails.Phone1 : contactdetails.Phone1);
                                param.Add("ipPhone2", !string.IsNullOrEmpty(objcontactdetails.Phone2) ? objcontactdetails.Phone2 : contactdetails.Phone2);
                                param.Add("ipEmail", !string.IsNullOrEmpty(objcontactdetails.Email) ? objcontactdetails.Email : contactdetails.Email);
                                param.Add("ipAddress", !string.IsNullOrEmpty(objcontactdetails.Address) ? objcontactdetails.Address : contactdetails.Address);
                                param.Add("ipStreet", !string.IsNullOrEmpty(objcontactdetails.Street) ? objcontactdetails.Street : contactdetails.Street);
                                param.Add("ipCity", !string.IsNullOrEmpty(objcontactdetails.City) ? objcontactdetails.City : contactdetails.City);
                                param.Add("ipState", !string.IsNullOrEmpty(objcontactdetails.State) ? objcontactdetails.State : contactdetails.State);
                                param.Add("ipzip", !string.IsNullOrEmpty(objcontactdetails.Zip) ? objcontactdetails.Zip : contactdetails.Zip);
                                param.Add("ipKeywords", !string.IsNullOrEmpty(objcontactdetails.KeyWords) ? objcontactdetails.KeyWords : contactdetails.KeyWords);
                                param.Add("ipCompanyType", !string.IsNullOrEmpty(objcontactdetails.CompanyType) ? objcontactdetails.CompanyType : contactdetails.CompanyType);
                                param.Add("ipPersonType", !string.IsNullOrEmpty(objcontactdetails.PersonType) ? objcontactdetails.PersonType : contactdetails.PersonType);
                                param.Add("outLID", direction: ParameterDirection.Output);
                                try
                                {

                                    var mpresults = dbExcel.QueryMultiple("contactdetails_crud", param, commandType: System.Data.CommandType.StoredProcedure);
                                    dbExcel.Close();
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine($"Updateduplicatedata => {ex}");
                                }
                            }
                            var addtionaldata = GetAddtionalDetails(contactdetails.ContactId);
                            foreach (var addcolumn in objadditionaldetails)
                            {

                                var additionaldetails = (addtionaldata != null) ? addtionaldata.Where(x => x.ContactId == contactdetails.ContactId && x.ColumnName.ToString().ToLower().Equals(addcolumn.ColumnName.ToLower())).Select(x => x).FirstOrDefault() : null;
                                if (additionaldetails != null && additionaldetails.Id != 0)
                                {
                                    var param = new DynamicParameters();
                                    param.Add("ipMode", "Updateaddtionaldetailsbycontactid");
                                    param.Add("ipId", additionaldetails.Id);
                                    param.Add("ipContactId", additionaldetails.ContactId);
                                    param.Add("ipColumnName", !string.IsNullOrEmpty(addcolumn.ColumnName) ? addcolumn.ColumnName : additionaldetails.ColumnName);
                                    param.Add("ipValue", !string.IsNullOrEmpty(addcolumn.ColumnValue) ? addcolumn.ColumnValue : additionaldetails.ColumnValue);
                                    param.Add("ipRowNumber", addcolumn.RowNumber);
                                    param.Add("outLID", direction: ParameterDirection.Output);
                                    try
                                    {
                                        var mpresults = dbExcel.QueryMultiple("addtionaldetails_crud", param, commandType: System.Data.CommandType.StoredProcedure);
                                        dbExcel.Close();
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                                else
                                {

                                    if (addcolumn.ColumnValue != null)
                                    {
                                        var param = new DynamicParameters();
                                        param.Add("ipMode", "InsertAdditionalDetails");
                                        param.Add("ipId", 0);
                                        param.Add("ipContactId", contactdetails.ContactId);
                                        param.Add("ipColumnName", addcolumn.ColumnName);
                                        param.Add("ipValue", addcolumn.ColumnValue);
                                        param.Add("ipRowNumber", index);
                                        param.Add("outLID", direction: ParameterDirection.Output);
                                        try
                                        {
                                            var mpresults = dbExcel.QueryMultiple("addtionaldetails_crud", param, commandType: System.Data.CommandType.StoredProcedure);
                                            dbExcel.Close();
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                    }
                                }
                                if (dbExcel != null)
                                    dbExcel.Close();
                            }
                        }
                    }
                    index++;
                }
                if (index > 0)
                    return Json("success");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }

        public DataTable ContactswithaddtionaldetailsRead()
        {
            List<Contacts> _Contacts = new List<Contacts>();
            List<AdditionalDetails> additionalColumns = new List<AdditionalDetails>();
            DataTable dt = new DataTable();
            try
            {
                var param = new DynamicParameters();
                param.Add("ipMode", "GetAll");
                param.Add("ipContactId", 0);
                param.Add("ipCompany", null);
                param.Add("ipLastName", null);
                param.Add("ipFirstName", null);
                param.Add("ipTitle", null);
                param.Add("ipPhone1", null);
                param.Add("ipPhone2", null);
                param.Add("ipEmail", null);
                param.Add("ipAddress", null);
                param.Add("ipStreet", null);
                param.Add("ipCity", null);
                param.Add("ipState", null);
                param.Add("ipzip", null);
                param.Add("ipKeywords", null);
                param.Add("ipCompanyType", null);
                param.Add("ipPersonType", null);
                param.Add("outLID", direction: ParameterDirection.Output);

                try
                {
                    using (var mpresults = dbExcel.QueryMultiple("contactdetails_crud", param, commandType: System.Data.CommandType.StoredProcedure))
                    {
                        _Contacts = mpresults.Read<Contacts>().ToList();
                        dbExcel.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"ContactswithaddtionaldetailsRead => {ex}");
                }

                var properties = typeof(Contacts).GetProperties();

                foreach (PropertyInfo p in properties)
                {
                    if (!p.Name.Contains("ContactId") && !p.Name.Contains("Status") && !(p.Name.Contains("AdditionalDetails"))
                        && !(p.Name.Contains("InsertedDate")) && !(p.Name.Contains("ModifiedDate")))
                    {
                        dt.Columns.Add(p.Name, p.PropertyType);
                    }
                }

                foreach (var data in _Contacts)
                {

                    var addparam = new DynamicParameters();

                    addparam.Add("ipMode", "Getaddtionaldetailsbycontactid");
                    addparam.Add("ipId", 0);
                    addparam.Add("ipContactId", data.ContactId);
                    addparam.Add("ipColumnName", null);
                    addparam.Add("ipValue", null);
                    addparam.Add("ipRowNumber", 0);
                    addparam.Add("outLID", direction: ParameterDirection.Output);
                    try
                    {
                        var mpresults = dbExcel.QueryMultiple("addtionaldetails_crud", addparam, commandType: System.Data.CommandType.StoredProcedure);
                        additionalColumns = mpresults.Read<AdditionalDetails>().ToList();
                        dbExcel.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    foreach (var v in additionalColumns)
                    {
                        if (!dt.Columns.Contains(v.ColumnName))
                            dt.Columns.Add(v.ColumnName, v.ColumnName.GetType());
                    }

                    var values = new object[dt.Columns.Count];

                    int index = 0;
                    for (int i = 0; i < properties.Length; i++)
                    {
                        if (!properties[i].Name.Contains("ContactId") && !properties[i].Name.Contains("Status")
                            && !properties[i].Name.Contains("AdditionalDetails") && !properties[i].Name.Contains("InsertedDate")
                            && !properties[i].Name.Contains("ModifiedDate"))
                        {
                            values[index] = properties[i].GetValue(data, null);
                            index++;
                        }
                    }

                    for (int j = index; j < dt.Columns.Count; j++)
                    {
                        int col = 0;
                        for (int x = index; x <= dt.Columns.Count; x++)
                        {
                            if (col < additionalColumns.Count() && dt.Columns[j].ColumnName.Contains(additionalColumns[col].ColumnName))
                            {
                                if (!string.IsNullOrEmpty(additionalColumns[col].ColumnValue))
                                    values[j] = additionalColumns[col].ColumnValue;
                            }
                            col++;
                        }


                    }

                    dt.Rows.Add(values);
                }

            }
            catch (Exception ex)
            {

            }
            return dt;
        }

        public ActionResult GetAllColumnNameList()
        {
            var columnNames = new List<string>();
            var param = new DynamicParameters();
            param.Add("ipMode", "staticcolumn");
            try
            {

                var mpresults = dbExcel.QueryMultiple("getallcolumnnamelist", param, commandType: System.Data.CommandType.StoredProcedure);
                columnNames = mpresults.Read<string>().ToList();
                columnNames.Remove("inserteddate");
                columnNames.Remove("modifieddate");
                dbExcel.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Updateduplicatedata => {ex}");
            }
            var newparam = new DynamicParameters();
            newparam.Add("ipMode", "dynamiccolumn");
            try
            {

                var mpresults = dbExcel.QueryMultiple("getallcolumnnamelist", newparam, commandType: System.Data.CommandType.StoredProcedure);
                var addtionalcolumnNames = mpresults.Read<string>().ToList();
                dbExcel.Close();
                if (addtionalcolumnNames.Count > 0)
                {
                    columnNames.AddRange(addtionalcolumnNames);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Updateduplicatedata => {ex}");
            }

            return Json(columnNames);
        }

    }
}