﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Excel_Import.Models
{
    public class Contacts
    {
        public int ContactId { get; set; }
        public string Company { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Title { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string KeyWords { get; set; }
        public string CompanyType { get; set; }
        public string PersonType { get; set; }
        public string InsertedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string Status { get; set; }
        //public AdditionalDetails[] AdditionalDetails { get; set; }
        public List<AdditionalDetails> AdditionalDetails { get; set; }
    }
  
    public class AdditionalDetails
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }
        public int RowNumber { get; set; }
        public string InsertedDate { get; set; }
        public string ModifiedDate { get; set; }
    }
}
