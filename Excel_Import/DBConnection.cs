﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Excel_Import
{
    public static class DBConnection
    {
        public static string DBConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MySqlConnStringExcel"].ToString();
        }
        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(DBConnectionString());
            }
        }
    }
}